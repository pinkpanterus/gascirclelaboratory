﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBeam : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject spawnObject;
    public Vector3 spawnValues;     //координаты в пределах которых происходит спавн объекта

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightControl))
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, Random.Range(-spawnValues.z, spawnValues.z));
            Instantiate(spawnObject, spawnPosition + spawnPoint.transform.position, Quaternion.identity);
        }
    }
}
