﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public List<AxleInfo> axInfos;
    public float maxMotorTorque;
    public float maxBrakeTorque = 300f;
    public float maxSteeringAngle;

    private float _motor;
    private bool _braking;
    private float steering;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _braking = true;
        }
        else
            _braking = false;
    }
    private void FixedUpdate()
    {
        if (!_braking)
        {
            _motor = maxMotorTorque * Input.GetAxis("Vertical");
        }
        else
            _motor = 0f;
        steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo axinfo in axInfos)
        {
            if (axinfo.steering)
            {
                axinfo.leftWheel.steerAngle = steering;  //угол поворота для левого колеса
                axinfo.rightWheel.steerAngle = steering;  //угол поворота для правого колеса
            }
            if (axinfo.motor)
            {
                axinfo.leftWheel.motorTorque = _motor;  //левое колесо вращается
                axinfo.rightWheel.motorTorque = _motor;  //правое колесо вращается
            }
            if (_braking)
            {
                axinfo.leftWheel.brakeTorque = maxBrakeTorque;
                axinfo.leftWheel.brakeTorque = maxBrakeTorque;
            }
            else
                axinfo.leftWheel.brakeTorque = 0f;
                axinfo.leftWheel.brakeTorque = 0f;
            ApplyLocalPositionToVisual(axinfo.leftWheel);   //вращение видимы колес
            ApplyLocalPositionToVisual(axinfo.rightWheel);
        }
        void ApplyLocalPositionToVisual(WheelCollider collider)
        {
            if (collider.transform.childCount == 0)
            {
                return;
            }
            Transform visualwheel = collider.transform.GetChild(0); //ссылка на компоненту Transform модели колеса
            Vector3 position;       
            Quaternion rotation;
            collider.GetWorldPose(out position, out rotation);

            visualwheel.transform.position = position;
            visualwheel.transform.rotation = rotation;
        }
    }
}
