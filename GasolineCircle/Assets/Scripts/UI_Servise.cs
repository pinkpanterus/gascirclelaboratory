﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GasoineCircle.GameManager;

namespace GasoineCircle
{
    public class UI_Servise : MonoBehaviour
    {
        [SerializeField] private Text player_1_Title;
        [SerializeField] private Text player_2_Title;

        [SerializeField] private Text player_1_LapsText;
        [SerializeField] private Text player_2_LapsText;
        //[SerializeField] private Text player_3_LapsText;
        //[SerializeField] private Text player_4_LapsText;

        [SerializeField] private GameObject MainMenuPanel;
        [SerializeField] private GameObject GamePanel;
        [SerializeField] private GameObject WinPanel;
        [SerializeField] private GameObject PrehistoryPanel;

        [SerializeField] private AudioClip buttonClickSound;
        [SerializeField] private AudioClip messageSound;
        [SerializeField] private AudioClip fireWeapon;
        [SerializeField] private AudioClip reloadWeapon;
        [SerializeField] private Image[] weaponReadyIcons;
        
        private AudioSource audiosource; 


        void Start()
        {
            audiosource = GetComponent<AudioSource>();

            GameManager.Instance.OnLapsChange += UpdateLaps;
            GameManager.Instance.OnMessageRaised += ShowMessage;
            GameManager.Instance.OnStateChange += ShowPanel;
            GameManager.Instance.onCarSpawned += SetPlayerNames;
            GameManager.Instance.onCarSpawned += PlayButtonClick;
            GameManager.Instance.onAmmoStatusChanged += ChangeAmmoIcon;
            GameManager.Instance.onCameraShakeRequest += ShakeCameraByRequest;
        }

        private void ShakeCameraByRequest()
        {
            EZCameraShake.CameraShaker.Instance.ShakeOnce(2,2,1,1);
        }

        private void ChangeAmmoIcon(int car_id, bool iconStatus)
        {
            if (weaponReadyIcons[car_id].enabled != iconStatus) 
            {
                if (weaponReadyIcons[car_id].enabled == true)
                {                   
                    audiosource.PlayOneShot(fireWeapon);
                }
                else 
                {
                    audiosource.PlayOneShot(reloadWeapon);
                }
                weaponReadyIcons[car_id].enabled = iconStatus;

            }
            
        }

        private void StartListenMessageScript()
        {
            Invoke("SubscriseToMessage", 0.5f);
        }

        private void SubscriseToMessage() 
        {
            MessageScript.Instance.onMessageSend += PlayMessageSound;
        }

        private void PlayMessageSound()
        {
            audiosource.PlayOneShot(messageSound);
        }

        private void PlayButtonClick()
        {
            audiosource.PlayOneShot(buttonClickSound);
        }

        private void ShowPanel(GameManager.GameState currentGameState)
        {
            switch (currentGameState)
            {
                case GameManager.GameState.MainMenu:
                    {
                        MainMenuPanel.SetActive(true);
                        GamePanel.SetActive(false);
                        WinPanel.SetActive(false);
                        PrehistoryPanel.SetActive(false);
                        Cursor.visible = true;
                    }
                    break;
                case GameManager.GameState.Game:
                    {
                        MainMenuPanel.SetActive(false);
                        GamePanel.SetActive(true);
                        WinPanel.SetActive(false);
                        PrehistoryPanel.SetActive(false);
                        Cursor.visible = false;

                        StartListenMessageScript();
                        //Invoke("SetPlayerNames",0.1f);
                    }
                    break;
                case GameState.Win:
                    {
                        MainMenuPanel.SetActive(false);
                        GamePanel.SetActive(false);
                        WinPanel.SetActive(true);
                        PrehistoryPanel.SetActive(false);
                        Cursor.visible = true;

                        WinPanel.GetComponentInChildren<Text>().text = GameManager.Instance.WinningPlayerName + " win!";
                    }
                    break;
                case GameState.Prehistory:
                    {
                        MainMenuPanel.SetActive(false);
                        GamePanel.SetActive(false);
                        WinPanel.SetActive(false);
                        PrehistoryPanel.SetActive(true);
                        Cursor.visible = false;
                    }                    
                    break;
                    //case GameState.Lose:
                    //    //Show Lose panel
                    //    break; 
            }
        }

        private void SetPlayerNames() 
        {
            //Debug.Log("Cars spawned event triggered");

            player_1_Title.GetComponent<Text>().text = GameManager.Instance.CarsSpawned[0].GetComponentInChildren<VehicleCustomeInput>().PlayerName;
            player_2_Title.GetComponent<Text>().text = GameManager.Instance.CarsSpawned[1].GetComponentInChildren<VehicleCustomeInput>().PlayerName;
        }

        private void ShowMessage(string message)
        {
            MessageScript.Instance.PopUp(message);
        }

        private void UpdateLaps(int car_id, int lapsCount)
        {
            switch (car_id) 
            {
                case 0:
                    player_1_LapsText.text = "Laps: " + lapsCount;
                    break;
                case 1:
                    player_2_LapsText.text = "Laps: " + lapsCount;
                    break;
                //case 3:
                //    player_3_LapsText.text = "Laps: " + lapsCount;
                //    break;
                //case 4:
                //    player_4_LapsText.text = "Laps: " + lapsCount;
                //    break;
            }
        }

        void Update()
        {
            if (GameManager.Instance.gameState != GameState.MainMenu)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    GameManager.Instance.MainMenu();
                }
            }

        }

        private void OnDestroy()
        {
            GameManager.Instance.OnLapsChange -= UpdateLaps;
            GameManager.Instance.OnMessageRaised -= ShowMessage;
            GameManager.Instance.OnStateChange -= ShowPanel;
            GameManager.Instance.onCarSpawned -= SetPlayerNames;
            GameManager.Instance.onCarSpawned -= PlayButtonClick;
        }
    }
}