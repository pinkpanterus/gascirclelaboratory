﻿using System;
using System.Linq;
using UnityEngine;


namespace GasoineCircle
{
    public class FinishScript : MonoBehaviour
    {
        [SerializeField] public Action<GameObject> onFinishTriggered;
        [SerializeField] private int[] cars_id;
        //[SerializeField] private VehicleCustomeInput[] nearestCars;
        
        void Start()
        {
            GameManager.Instance.OnStateChange += RegisterCArOnStartGame;
            //Invoke("RegisterCarToFinishLine",0.5f);
        }

        private void RegisterCArOnStartGame(GameManager.GameState currentGameState)
        {
            if (currentGameState == GameManager.GameState.Game) 
            {
                Invoke("RegisterCarToFinishLine", 0.5f);
            }
        }

        private void RegisterCarToFinishLine()
        {
            VehicleCustomeInput[] cars = GameObject.FindObjectsOfType<VehicleCustomeInput>().Where(x => Vector3.Distance(x.transform.position, transform.position) <= 30).ToArray();
            //nearestCars = cars.Take(2).ToArray();
            cars_id = new int[cars.Length];

            for (int i = 0; i <cars.Length; i++)
            {
                cars_id[i] = cars[i].ID;
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {            
            if (other.tag == "Player" && cars_id.Contains(other.GetComponent<VehicleCustomeInput>().ID) /*|| other.tag == "Bot"*/)
            {
                onFinishTriggered?.Invoke(other.gameObject);
                Debug.Log("Finish triggered by player " + other.gameObject.GetComponent<VehicleCustomeInput>().ID);
            }
        }
    }
}