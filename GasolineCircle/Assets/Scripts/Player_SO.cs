﻿using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "ScriptableObjects/Player", order = 1)]
public class Player_SO : ScriptableObject
{
    public int playerID;
    public string playerName;
    public int bombAmount;
    public GameObject bombPrefab;
}
