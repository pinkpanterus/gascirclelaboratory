﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GasoineCircle
{
    public class DestryByTime : MonoBehaviour
    {
        public float timeBeforeDestroy = 4f;

        // Start is called before the first frame update
        void Start()
        {
            Destroy(this.gameObject, timeBeforeDestroy);
        }
     
    }
}
