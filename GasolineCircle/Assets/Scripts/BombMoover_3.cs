﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GasoineCircle
{
    public class BombMoover_3 : MonoBehaviour
    {
        public Vector3 targetPos;


        public float thrust = 100.0f;
        public Rigidbody rb;
        //public float speed = 500f;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        //void Update()
        //{
        //    //transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * speed);

        //    transform.LookAt(targetPos, -Vector3.forward);


        //    // transform.Translate(Vector3.forward * speed * Time.deltaTime);
        //    //transform.LookAt(targetPos, Vector3.up);
        //}

        void FixedUpdate()
        {
            transform.LookAt(targetPos, Vector3.forward);
            rb.AddForce(transform.forward * thrust);
        }


        void OnDrawGizmos()
        {
            // Draw a yellow sphere at the transform's position
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(targetPos, 2);

        }
    }
}