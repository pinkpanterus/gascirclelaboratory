﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GasoineCircle
{
    public class CarSensors : MonoBehaviour
    {
        public Action<int, int> onCheckTrack;
		[SerializeField] private int id;
		
		private void Start()
		{
			id = GetComponent<VehicleCustomeInput>().ID;
			InvokeRepeating("CheckTrack", 0.1f, 0.5f);
		}



		void CheckTrack()
		{
			int layerMask = 1 << 10; // tracks layer mask = 10. Shift the index of the layer (10) to get a bit mask.
			RaycastHit hit;

			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up), out hit, 10, layerMask))
			{
				// подписки почему-то не срабатывали
				//onCheckTrack?.Invoke(hit.collider.gameObject.GetComponent<TrackScript>().TrackID, id);

				//Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * hit.distance, Color.yellow);
				//Debug.Log(hit.collider.gameObject.GetComponent<TrackScript>().TrackID);

				// пришлось делать напрямую
				GameManager.Instance.CurrentTrack[id] = hit.collider.gameObject.GetComponent<TrackScript>().TrackID;
			}
			else
			{
				// подписки почему-то не срабатывали
				//onCheckTrack?.Invoke(0, id);

				//Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * 1000, Color.white);
				//Debug.Log("Did not Hit");

				// пришлось делать напрямую
				GameManager.Instance.CurrentTrack[id] = 0;
			}
		}
	}
}
