﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GasoineCircle
{
    public class WeaponScript : MonoBehaviour
    {
        [SerializeField] private GameObject explosionVFX;
        [SerializeField] float radius = 5;        
        public Action<GameObject> onDamage;
        public Action<GameObject> onWeaponRemovedFromScene;
        public Action onShakeCamera;
        private List<Collision> collisions = new List<Collision>();


        // Start is called before the first frame update
        void Start()
        {
           
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnCollisionEnter(Collision collision)
        {
            onShakeCamera?.Invoke();

            if (!collisions.Contains(collision))
            {
                collisions.Add(collision);

                List<GameObject> messagedGameobject = new List<GameObject>();

                if (TryGetComponent(out Fracture fracture))
                {
                    fracture.FractureObject();
                }

                Instantiate(explosionVFX, transform.position, Quaternion.identity);
                Destroy(this.gameObject);

                Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius).Distinct().ToArray();

                foreach (var hitCollider in hitColliders)
                {
                    if (hitCollider.gameObject.layer == 9 && !messagedGameobject.Contains(hitCollider.gameObject)) // car layer
                    {
                        onDamage?.Invoke(hitCollider.gameObject);
                        messagedGameobject.Add(hitCollider.gameObject);
                    }
                }

                messagedGameobject.Clear();

                collisions.Clear();
            }
            
        }

        private void OnDestroy()
        {
            onWeaponRemovedFromScene?.Invoke(this.gameObject);
        }
    }
}
